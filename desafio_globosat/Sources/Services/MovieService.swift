//
//  MovieService.swift
//  desafio_globosat
//
//  Created by Victor Yves Crispim on 30/11/17.
//  Copyright © 2017 Victor Yves Crispim. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

protocol MovieServiceDelegate: class {
	func serviceDidFinishSearch(for title: String, with results: [Movie])
	func serviceDidFinishSearch(for title: String, with error: Error)
	func serviceDidFinishDownloading(movieData: Movie, for id: String)
	func serviceDidFinishDownloadingData(for movieID: String, with error: Error)
}

class MovieService {
	private let baseURL = "http://www.omdbapi.com/?apikey=c830bb27&type=movie"
	weak var delegate: MovieServiceDelegate?
	
	func startSeatchingForMovie(with title: String) {
		let url = self.addQueryParameters(["s":title], to: self.baseURL)
		Alamofire.request(url).responseArray(keyPath: "Search") { [weak self] (response: DataResponse<[Movie]>) in
			if let movies = response.value, response.error == nil {
				
				for movie in movies {
					let utilityQueue = DispatchQueue.global(qos: .utility)
					if let posterString = movie.poster, let posterURL = URL(string: posterString) {
						Alamofire.request(posterURL).responseData(queue: utilityQueue) { [weak self] (response: DataResponse<Data>) in
							if let imageData = response.data {
								movie.posterThumbnail = UIImage(data: imageData)
								DispatchQueue.main.async {
									self?.delegate?.serviceDidFinishSearch(for: title, with: movies)
								}
							} else {
								DispatchQueue.main.async {
									self?.delegate?.serviceDidFinishSearch(for: title, with: movies)
								}
							}
						}
					}
				}
			} else {
				self?.delegate?.serviceDidFinishSearch(for: title, with: response.error!)
			}
		}
	}
	
	func getFullData(for movieID: String) {
		let url = self.addQueryParameters(["i":movieID], to: self.baseURL)
		Alamofire.request(url).responseObject { [weak self] (response: DataResponse<Movie>) in
			if let movie = response.value, response.error == nil {
				let utilityQueue = DispatchQueue.global(qos: .utility)
				if let posterString = movie.poster, let posterURL = URL(string: posterString) {
					Alamofire.request(posterURL).responseData(queue: utilityQueue) { [weak self] (response: DataResponse<Data>) in
						if let imageData = response.data {
							movie.posterThumbnail = UIImage(data: imageData)
							DispatchQueue.main.async {
								self?.delegate?.serviceDidFinishDownloading(movieData: movie, for: movieID)
							}
						} else {
							DispatchQueue.main.async {
								self?.delegate?.serviceDidFinishDownloading(movieData: movie, for: movieID)
							}
						}
					}
				}
			} else {
				self?.delegate?.serviceDidFinishDownloadingData(for: movieID, with: response.error!)
			}
		}
	}
	
	private func addQueryParameters(_ parameters: [String: String], to url: String) -> String {
		if parameters.count == 0 { return "" }
		
		var queryString = url.contains("?") ? "&" : "?"
		for parameter in parameters {
			guard let scapedKey = parameter.key.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { continue }
			guard let scapedValue = parameter.value.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { continue }
			queryString += "\(scapedKey)=\(scapedValue)&"
		}
		queryString.remove(at: queryString.index(before: queryString.endIndex))
		return url + queryString
	}
}
