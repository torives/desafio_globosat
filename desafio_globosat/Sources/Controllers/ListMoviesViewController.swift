//
//  ViewController.swift
//  desafio_globosat
//
//  Created by Victor Yves Crispim on 29/11/17.
//  Copyright © 2017 Victor Yves Crispim. All rights reserved.
//

import UIKit

class ListMoviesViewController: UIViewController {

	@IBOutlet weak var collectionView: UICollectionView!
	
	fileprivate var movies: [Movie] = []
	lazy var service = MovieService()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupCollectionView()
		
		self.service.delegate = self
		
		//TODO: implement search
		service.startSeatchingForMovie(with: "Day")
	}
	
	private func setupCollectionView() {
		self.collectionView.delegate = self
		self.collectionView.dataSource = self
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		
		if segue.identifier == Strings.Identifiers.Segues.showMovieDetail {
			let vc = segue.destination as? MovieDetailViewController
			if let index = self.selectedCellIndex() {
				vc?.movie = self.movies[index.row]
			}
		}
	}
	
	private func selectedCellIndex() -> IndexPath? {
		guard let indexPaths = self.collectionView.indexPathsForSelectedItems else { return nil }
		return indexPaths.first
	}
}

//MARK: - UICollectionViewDataSource
extension ListMoviesViewController: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.movies.count
	}
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Strings.Identifiers.Cells.moviewCell, for: indexPath) as? MovieCell {
			cell.title.text = self.movies[indexPath.row].title
			cell.thumbnail.image = self.movies[indexPath.row].posterThumbnail
			return cell
		}
		return UICollectionViewCell()
	}
}

//MARK: - UICollectionViewDelegate
extension ListMoviesViewController: UICollectionViewDelegate {
	
}

//MARK: - MovieServiceDelegate
extension ListMoviesViewController: MovieServiceDelegate {
	
	func serviceDidFinishSearch(for title: String, with results: [Movie]) {
		self.movies = results
		self.collectionView.reloadData()
	}
	
	func serviceDidFinishSearch(for title: String, with error: Error) {
		let alert = UIAlertController(title: Strings.Interface.Alerts.movieSearchDidFailTitle,
									  message: error.localizedDescription, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: Strings.Interface.Buttons.close, style: .cancel))
		self.present(alert, animated: true, completion: nil)
	}
	
	func serviceDidFinishDownloading(movieData: Movie, for id: String) {}
	
	func serviceDidFinishDownloadingData(for movieID: String, with error: Error) {}
}
