//
//  MovieDetailViewController.swift
//  desafio_globosat
//
//  Created by Victor Yves Crispim on 30/11/17.
//  Copyright © 2017 Victor Yves Crispim. All rights reserved.
//

import UIKit

class MovieDetailViewController: UITableViewController {

	@IBOutlet weak var thumbnail: UIImageView!
	@IBOutlet weak var movieTitle: UILabel!
	@IBOutlet weak var runtime: UILabel!
	@IBOutlet weak var plot: UILabel!
	
	var movie: Movie?
	lazy var service = MovieService()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.service.delegate = self
		
		if let id = movie?.id {
			self.service.getFullData(for: id)
		}
		self.updateContent()
	}
	
	fileprivate func updateContent() {
		self.movieTitle.text = self.movie?.title
		self.runtime.text = self.movie?.runtime
		self.plot.text = self.movie?.plot
		self.thumbnail.image = self.movie?.posterThumbnail
	}
}

extension MovieDetailViewController: MovieServiceDelegate {
	func serviceDidFinishSearch(for title: String, with results: [Movie]) {}
	
	func serviceDidFinishSearch(for title: String, with error: Error) {}
	
	func serviceDidFinishDownloading(movieData: Movie, for id: String) {
		self.movie = movieData
		self.updateContent()
	}
	
	func serviceDidFinishDownloadingData(for movieId: String, with error: Error) {
		
	}
}
