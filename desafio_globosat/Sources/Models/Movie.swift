//
//  Movie.swift
//  desafio_globosat
//
//  Created by Victor Yves Crispim on 30/11/17.
//  Copyright © 2017 Victor Yves Crispim. All rights reserved.
//

import Foundation
import ObjectMapper

class Movie: Mappable {
	var id: String?
	var title: String?
	var runtime: String?
	var awards: String?
	var releaseDate: String?
	var rating: String?
	var genre: String?
	var director: String?
	var plot: String?
	var poster: String?
	var posterThumbnail: UIImage?
	
	required init?(map: Map) {}
	
	func mapping(map: Map) {
		self.id <- map["imdbID"]
		self.title <- map["Title"]
		self.runtime <- map["Runtime"]
		self.awards <- map["Awards"]
		self.releaseDate <- map["Released"]
		self.rating <- map["Rated"]
		self.genre <- map["Genre"]
		self.director <- map["Director"]
		self.plot <- map["Plot"]
		self.poster <- map["Poster"]
	}
}
