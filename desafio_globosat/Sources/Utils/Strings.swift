//
//  Strings.swift
//  desafio_globosat
//
//  Created by Victor Yves Crispim on 30/11/17.
//  Copyright © 2017 Victor Yves Crispim. All rights reserved.
//

import Foundation

struct Strings {
	struct Identifiers {
		struct Segues {
			static let showMovieDetail = "showMovieDetail"
		}
		struct Cells {
			static let moviewCell = "movieCell"
		}
	}
	struct Interface {
		struct Alerts {
			static let movieSearchDidFailTitle = "Error"
		}
		struct Buttons {
			static let close = "Close"
		}
		
	}
}
