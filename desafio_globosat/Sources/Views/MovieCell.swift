//
//  MovieCell.swift
//  desafio_globosat
//
//  Created by Victor Yves Crispim on 30/11/17.
//  Copyright © 2017 Victor Yves Crispim. All rights reserved.
//

import UIKit

class MovieCell: UICollectionViewCell {
	
	@IBOutlet weak var thumbnail: UIImageView!
	@IBOutlet weak var title: UILabel!
	
	
}
